namespace BowlingGame.Models;

public class FrameModel {
    public bool IsFinal { get; set; }
    public bool IsStrike { get; set; }
    public bool IsSpare { get; set; }
    public int FirstRoll { get; set; }
    public int SecondRoll { get; set; }    
    public int ThirdRoll { get; set; }
    public int Score { get; set; }
}
