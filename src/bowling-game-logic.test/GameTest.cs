using System;
using Xunit;
using BowlingGame.Logic.Services;
using BowlingGame.Logic.Interfaces;
using BowlingGame.Models;


namespace bowling_game_logic.test;

public class GameTest {

    [Fact]
    public void Test_Game_Score() 
    {
        //Arrange 
        IFrame frame = new Frame();
        Game game = new Game(frame); 
        FrameModel[] frames = new FrameModel[10]; 
        frames[0] = new FrameModel {
            FirstRoll = 4, 
            SecondRoll = 3,
            IsFinal = false,
            IsStrike = false, 
            IsSpare = false,
        };

        frames[1] = new FrameModel {
            FirstRoll = 7,
            SecondRoll = 3,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = true,
        };

        frames[2] = new FrameModel {
            FirstRoll = 5,
            SecondRoll = 2,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = false,
        };

        frames[3] = new FrameModel {
            FirstRoll = 8,
            SecondRoll = 1,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = false,
        };

        frames[4] = new FrameModel {
            FirstRoll = 4, 
            SecondRoll = 6,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = true,
        };

        frames[5] = new FrameModel {
            FirstRoll = 2, 
            SecondRoll = 4, 
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = false,
        };

        frames[6] = new FrameModel {
            FirstRoll = 8, 
            SecondRoll = 0,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = false,
        };
        
        frames[7] = new FrameModel {
            FirstRoll = 8, 
            SecondRoll = 0,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = false,
        };

        frames[8] = new FrameModel {
            FirstRoll = 8, 
            SecondRoll = 2,
            IsFinal = false, 
            IsStrike = false, 
            IsSpare = true,
        };

        frames[9] = new FrameModel {
            FirstRoll = 10, 
            SecondRoll = 1, 
            ThirdRoll = 7,
            IsFinal = true, 
            IsStrike = true, 
            IsSpare = false,
        };

        //Act 
        int score = game.CalculateScore(frames);

        //Assert 
        Assert.Equal(110, score);
    }
}