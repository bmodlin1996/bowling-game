using System;
using Xunit;
using BowlingGame.Logic.Services;
using BowlingGame.Models;

namespace bowling_game_logic.test;

public class FrameTest
{
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(4)]
    [InlineData(5)]
    [InlineData(6)]
    [InlineData(7)]
    [InlineData(8)]
    [InlineData(9)]
    public void Final_Frame_False_Test_Pass(uint i)
    {
        //Arrange 
        Frame game = new Frame(); 

        //Act 
        bool result = game.IsFinalFrame(i);
        
        //Assert
        Assert.False(result);
    }

    [Theory]
    [InlineData(10)]
    public void Final_Frame_True_Test_Pass(uint i)
    {
        //Arrange 
        Frame game = new Frame(); 

        //Act
        bool result = game.IsFinalFrame(i);

        //Assert
        Assert.True(result);
    }

    [Theory]
    [InlineData(0)]
    public void Final_Frame_Less_Than_Zero_Exceptions(uint i)
    {
         //Arrange 
        Frame game = new Frame(); 

        //Act 
        void act() => game.IsFinalFrame(i);

        //Assert
        var exception = Assert.Throws<Exception>(act);
        Assert.Equal("Out of Bounds:  Frames must be greater than 0.", exception.Message);
    }

    [Theory]
    [InlineData(11)]
    [InlineData(20)]
    public void Final_Frame_Greater_Than_Ten_Exception(uint i)
    {
          //Arrange 
        Frame game = new Frame(); 

        //Act 
        void act() => game.IsFinalFrame(i);

        //Assert
        var exception = Assert.Throws<Exception>(act);
        Assert.Equal("Out of Bounds:  Frames must be less than 10.", exception.Message);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(8)]
    public void Next_Frame_Pass(uint i)
    {
        //Arrange 
        Frame frame = new Frame();

        //Act 
        uint next = frame.Next(i);

        //Assert 
        Assert.Equal((i + 1), next);
    }
    
    [Theory]
    [InlineData(10)]
    [InlineData(15)]
    public void Next_Frame_Fail(uint i) 
    {
        //Arrange
        Frame frame = new Frame();
        
        //Act 
        void act() =>  frame.Next(i);

        //Assert
        var exception = Assert.Throws<Exception>(act);
        Assert.Equal("Game Over.", exception.Message);
    }
    
}