namespace BowlingGame.Logic.Interfaces;

public interface IFrame 
{

    bool IsFinalFrame(uint Frame);
    
}
