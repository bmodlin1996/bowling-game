using BowlingGame.Models;
namespace BowlingGame.Logic.Interfaces;


public interface IGame 
{
    int RollBall(int pinsRemaining);
    FrameModel[] PlayGame(); 
    int CalculateScore(FrameModel[] frameModel);
}
