using BowlingGame.Logic.Interfaces;
using BowlingGame.Models; 
namespace BowlingGame.Logic.Services;


public class Frame : IFrame 
{

    public uint Next(uint frameNumber) 
    {
        uint nextFrame = frameNumber + 1;
        if(nextFrame >= 10)
        {
            throw new Exception("Game Over."); 
        }

        return nextFrame; 
    }

    public bool IsFinalFrame(uint frame) 
    {
        if(frame > 10)
        {
            throw new Exception("Out of Bounds:  Frames must be less than 10."); 
        }

        if(frame < 1)
        {
            throw new Exception("Out of Bounds:  Frames must be greater than 0.");
        }

        return frame == 10;
    }

   
}