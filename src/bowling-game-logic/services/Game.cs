using BowlingGame.Logic.Interfaces;
using BowlingGame.Models;
using System;
using System.Linq;

namespace BowlingGame.Logic.Services; 
public class Game : IGame 
{

    private readonly IFrame frame; 
    public const int TOTAL_PINS = 10;

    public Game(IFrame frame)
    {
        this.frame = frame;
    }

    public FrameModel[] PlayGame() 
    {
        FrameModel[] frames = new FrameModel[10];
        for(int i = 0; i < frames.Count(); i++)
        {
            FrameModel f = new FrameModel();
            uint currentFrameCount = (uint) i + 1;
            f.IsFinal = frame.IsFinalFrame((uint) currentFrameCount);
            int pinsRemaining; 
            int secondRoll;
            int firstRoll;
            int thirdRoll; 
            
            // First roll
            firstRoll = RollBall(TOTAL_PINS); 
            f.FirstRoll = firstRoll; 
            f.IsStrike = IsStrike(firstRoll);

            // If the first roll is a strike and the current frame is not the final frame skip second roll.
            // Else if roll is a strike and the current is the last frame roll for a second and third roll. 
            if(!f.IsStrike && !f.IsFinal)
            {
                pinsRemaining = (int) (TOTAL_PINS - firstRoll); 
                secondRoll = RollBall(pinsRemaining);
                f.SecondRoll = secondRoll; 
                f.IsSpare = IsSpare(firstRoll, secondRoll);
            } 
            else if(f.IsFinal && (f.IsStrike || f.IsSpare))
            {
                int remaining = (int) (TOTAL_PINS - f.SecondRoll);
                thirdRoll = (int) RollBall(remaining);
                f.ThirdRoll = thirdRoll;
            }

            frames[i] = f;
        }
        return frames;
    }

    public int CalculateScore(FrameModel[] frameModel) 
    {

        for(int i = 0; i <= (frameModel.Count() - 1); i++)
        {
            FrameModel frame = frameModel[i];
            if(frame.IsStrike && !frame.IsFinal)
            {
                int nextFrameIndex = i + 1; 
                FrameModel nextFrame = frameModel[nextFrameIndex];
                if(nextFrame.IsStrike)
                {
                    int thirdFrameIndex = nextFrameIndex + 1; 
                    FrameModel thirdFrame = frameModel[thirdFrameIndex];
                    frame.Score = frame.FirstRoll + nextFrame.FirstRoll + thirdFrame.FirstRoll;
                }
                else 
                {
                    frame.Score = frame.FirstRoll + nextFrame.FirstRoll + nextFrame.SecondRoll;
                }
            }
            else if(frame.IsStrike && frame.IsFinal)
            {
                frame.Score = frame.FirstRoll + frame.SecondRoll + frame.ThirdRoll;
            }
            else if(frame.IsSpare)
            {
                int nextFrameIndex = i + 1; 
                FrameModel nextFrame = frameModel[nextFrameIndex];
                frame.Score = frame.FirstRoll + frame.SecondRoll + nextFrame.FirstRoll;
            }
            else 
            {
                frame.Score = frame.FirstRoll + frame.SecondRoll; 
            }
        }

        return frameModel.Select(x => x.Score).Sum();
    }

    public int RollBall(int pinsRemaining) 
    {
        Random rand = new Random(); 
        int pinsHit = (int) rand.Next(0, pinsRemaining);
        return pinsHit;
    }

    

    private bool IsStrike(int firstRole) 
    {
        return firstRole == 10; 
    } 

    private bool IsSpare(int firstRole, int secondRole) 
    {
        return (firstRole + secondRole) == 10;
    }

    private int CalculateSpare(int role) 
    {
        return 10 + role; 
    }

    private int CalculateStrike(int firstRole, int secondRole)
    {
        return 10 + firstRole + secondRole; 
    }
}   
