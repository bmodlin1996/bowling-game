﻿using BowlingGame.Logic.Interfaces;
using BowlingGame.Logic.Services;
using BowlingGame.Models;

// See https://aka.ms/new-console-template for more information
IFrame frame = new Frame();
IGame game = new Game(frame);
Console.WriteLine("Welcome!  Would you like to play bowing? [y/n]");
char response = Console.ReadKey().KeyChar;
Console.WriteLine("\n");
if(response == 'y' || response == 'Y')
{
    FrameModel[] frames = game.PlayGame();
    game.CalculateScore(frames);
    Console.Write("|");
    for(int i = 0; i < frames.Count(); i++) 
    {
        int frameCount = i + 1;
        Console.Write($@"|  Frame {frameCount}  |");
    }
    Console.Write("|\n|");
    foreach(FrameModel f in frames)
    {
        if(!f.IsFinal){
            if(f.IsStrike)
            {
                Console.Write("|     X     |");
            }
            else if(f.IsSpare)
            {
                Console.Write("|     /     |");
            }
            else
            {
                Console.Write($"|   {f.FirstRoll}-{f.SecondRoll}     |");
            }
        }
        else
        {
            if(f.ThirdRoll <= 0)
            {
                Console.Write($"|   {f.FirstRoll}-{f.SecondRoll}-{f.ThirdRoll}    |");
            }
            else
            {
                Console.Write($"|   {f.FirstRoll}-{f.SecondRoll}      |");
            }
        }
    }
    Console.Write($"|  Score: {game.CalculateScore(frames)}  |");
} 
else if(response == 'n'){
    Console.WriteLine("Ok, maybe next time.");
}